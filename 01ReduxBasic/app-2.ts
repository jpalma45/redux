import { contadorReducer } from './contador/contado.reducer';
import { incrementadorAction, decrementadorAction,
         multiplicarAction, dividirAction,
         resetAction } from "./contador/contador.action";



 console.log(contadorReducer( 10, incrementadorAction ));

 console.log(contadorReducer( 10, decrementadorAction ));

 console.log(contadorReducer( 10, multiplicarAction ));

 console.log(contadorReducer( 10, dividirAction ));

 console.log(contadorReducer( 10, resetAction ));