import { Store, createStore } from 'redux';
import { contadorReducer } from './contador/contado.reducer';
import { incrementadorAction } from './contador/contador.action';

const store: Store = createStore( contadorReducer );

store.subscribe(() => {
    console.log('Subs', store.getState());
});

store.dispatch( incrementadorAction );