// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBbFOuxJbk2MpOHXev3LbVrwTNBLnc3EdI',
    authDomain: 'ingreso-egreso-app-d1838.firebaseapp.com',
    databaseURL: 'https://ingreso-egreso-app-d1838.firebaseio.com',
    projectId: 'ingreso-egreso-app-d1838',
    storageBucket: 'ingreso-egreso-app-d1838.appspot.com',
    messagingSenderId: '700191203686',
    appId: '1:700191203686:web:6eb3125e17d8ae5253a8a7',
    measurementId: 'G-XERF6DBTJ3'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
